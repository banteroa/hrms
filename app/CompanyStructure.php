<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyStructure extends Model
{
    //
    protected $fillable = [
        'name', 'description', 'address', 'type_id', 'parent_id'
    ];

    public function getTypeIdParent(){
        return $this->belongsTo('App\CompanyStructureType','type_id');
    }

    public function getParent(){
        return $this->belongsTo('App\CompanyStructure','parent_id');
    }

    public function getChildren(){
        return $this->hasMany('App\CompanyStructure','parent_id');
    }
}
