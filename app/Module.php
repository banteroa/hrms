<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    //

    protected $fillable = [
        'module_name', 'is_enabled','description'
    ];

    public function getSubModules(){
        return $this->hasMany('App\SubModule','module_id')->where('is_enabled',1);
    }

    public function getModules(){
        return $this::all();
    }
}
