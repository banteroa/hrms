<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class GlobalModule extends Model
{
    //
    protected $modules;
    protected $keyValuePair;

    public function __construct(Collection $modules)
    {
        $this->modules = $modules;
        foreach ($modules as $module){
            $this->keyValuePair[$module->key] = $module->value;
        }
    }

    public function has(string $key){ /* check key exists */ }
    public function contains(string $key){ /* check value exists */ }
    public function getModules(){
        return $this->modules;
    }
    public function get(string $key){
        return $this->modules[$key];
        /* get by key */
    }
}
