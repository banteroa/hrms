<?php

namespace App\Providers;

use App\GlobalModule;
use App\Module;
use function foo\func;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton('App\GlobalModule',function ($app){
            return new GlobalModule(Module::where('is_enabled',1)->get());
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(GlobalModule $globalModule)
    {
        //
        View::share('globalModules',$globalModule);
    }
}
