<?php

namespace App\Http\Controllers;

use App\Module;
use App\SubModule;
use Illuminate\Http\Request;

class SubModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sub_modules = SubModule::where('is_enabled',1)->get();
        return view('settings.modules.index',['sub_modules' => $sub_modules]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubModule  $subModule
     * @return \Illuminate\Http\Response
     */
    public function show(SubModule $subModule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubModule  $subModule
     * @return \Illuminate\Http\Response
     */
    public function edit(SubModule $subModule)
    {
        //
        $modules = Module::all();
        return view('settings.modules.edit',['sub_module' => $subModule,'modules' => $modules]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubModule  $subModule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubModule $subModule)
    {
        $response = array();
        try{
            $subModule->update([
                'module_id' => $request->input('module_id'),
                'submodule_name' => $request->input('submodule_name'),
                'is_enabeld' => $request->input('is_enabled'),
                'route' => $request->input('route')
            ]);
            $response = [
                'success' => true,
                'message' => 'Sub Module updated!'
            ];
        }catch (\Exception $exception){
            $response = [
                'success' => false,
                'message' => $exception->getMessage()
            ];
        }

        return response()->json($response,200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubModule  $subModule
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubModule $subModule)
    {
        //
    }
}
