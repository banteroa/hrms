<?php

namespace App\Http\Controllers;

use App\CompanyStructure;
use App\CompanyStructureType;
use Illuminate\Http\Request;

class CompanyStructureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $structures = CompanyStructure::all();
        return view('admin.company.index',['structures' => $structures]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $types = CompanyStructureType::all();
        $parents = CompanyStructure::all();
        return view('admin.company.create',['types' => $types,'parents' => $parents]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            CompanyStructure::create([
                'name' => $request->input('name'),
                'description' => $request->input('details'),
                'address' => $request->input('address'),
                'type_id' => $request->input('type'),
                'parent_id' => $request->input('parent_structure')
            ]);
            $response = [
                'success' => true,
                'message' => 'Company Structure created!'
            ];
        }catch (\Exception $exception){
            $response = [
                'success' => false,
                'message' => $exception->getMessage()
            ];
        }

        return response()->json($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyStructure  $companyStructure
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyStructure $companyStructure)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyStructure  $companyStructure
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyStructure $companyStructure)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyStructure  $companyStructure
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyStructure $companyStructure)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyStructure  $companyStructure
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyStructure $companyStructure)
    {
        //
    }
}
