<?php

namespace App\Http\Controllers;

use App\EmployeeSocialMedia;
use Illuminate\Http\Request;

class EmployeeSocialMediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeSocialMedia  $employeeSocialMedia
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeSocialMedia $employeeSocialMedia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeSocialMedia  $employeeSocialMedia
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeSocialMedia $employeeSocialMedia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeSocialMedia  $employeeSocialMedia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeSocialMedia $employeeSocialMedia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeSocialMedia  $employeeSocialMedia
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeSocialMedia $employeeSocialMedia)
    {
        //
    }
}
