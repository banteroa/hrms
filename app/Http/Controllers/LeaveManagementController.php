<?php

namespace App\Http\Controllers;

use App\LeaveManagement;
use Illuminate\Http\Request;

class LeaveManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.leave_management.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LeaveManagement  $leaveManagement
     * @return \Illuminate\Http\Response
     */
    public function show(LeaveManagement $leaveManagement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LeaveManagement  $leaveManagement
     * @return \Illuminate\Http\Response
     */
    public function edit(LeaveManagement $leaveManagement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LeaveManagement  $leaveManagement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LeaveManagement $leaveManagement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LeaveManagement  $leaveManagement
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeaveManagement $leaveManagement)
    {
        //
    }
}
