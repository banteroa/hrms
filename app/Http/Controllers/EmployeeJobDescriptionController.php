<?php

namespace App\Http\Controllers;

use App\EmployeeJobDescription;
use Illuminate\Http\Request;

class EmployeeJobDescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeJobDescription  $employeeJobDescription
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeJobDescription $employeeJobDescription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeJobDescription  $employeeJobDescription
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeJobDescription $employeeJobDescription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeJobDescription  $employeeJobDescription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeJobDescription $employeeJobDescription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeJobDescription  $employeeJobDescription
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeJobDescription $employeeJobDescription)
    {
        //
    }
}
