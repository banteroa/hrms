<?php

namespace App\Http\Controllers;

use App\EmployeeWorkExperience;
use Illuminate\Http\Request;

class EmployeeWorkExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeWorkExperience  $employeeWorkExperience
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeWorkExperience $employeeWorkExperience)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeWorkExperience  $employeeWorkExperience
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeWorkExperience $employeeWorkExperience)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeWorkExperience  $employeeWorkExperience
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeWorkExperience $employeeWorkExperience)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeWorkExperience  $employeeWorkExperience
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeWorkExperience $employeeWorkExperience)
    {
        //
    }
}
