<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubModule extends Model
{
    //
    protected $fillable = [
        'module_id', 'submodule_name','is_enabled','user_group','route'
    ];

    public function getModuleParent(){
        return $this->belongsTo('App\Module','module_id');
    }
}
