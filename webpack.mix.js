const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
    .styles([
        'public/css/jquery-ui.custom.min.css',
        'public/css/entypo.css',
        'public/css/bootstrap.css',
        'public/css/neon-core.css',
        'public/css/neon-theme',
        'public/css/neon-forms.css',
        'public/css/custom.css',
        'public/css/datatables.css',
        'public/css/font-awesome.min.css'
    ],'public/css/login.css')
    .scripts([
        'public/js/jquery.min.js',
        'public/js/TweenMax.min.js',
        'public/js/jquery-ui-minimal.min.js',
        'public/js/bootstrap.js',
        'public/js/joinable.js',
        'public/js/resizeable.js',
        'public/js/neon-api.js',
        'public/js/jquery.bootstrap.wizard.min.js',
        'public/js/jquery.validate.min.js',
        'public/js/neon-login.js',
        'public/js/neon-custom.js',
        'public/js/neon-demo.js',
        'public/js/datatables.js',
        'public/js/toastr.js'
    ],'public/js/login.js');
