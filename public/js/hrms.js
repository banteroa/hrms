
function toastMessage(title,message){
    var opts = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    toastr.error(message, title, opts);
}
// $('#new_structure_form').validate({
//      highlight: function(element){
//         $(element).closest('.form-group').addClass('validate-has-error');
//     },
//
//
//     unhighlight: function(element)
//     {
//         $(element).closest('.form-group').removeClass('validate-has-error');
//     },
//
//     submitHandler: function(ev)
//     {
//
//     }
// });
$(document).on('click','.edit_button',function (e) {
    e.preventDefault();
    let url = $(this).data('url');
    let title = $(this).data('title');
    $.ajax({
        url: url,
        type: 'GET',
        success: function (data) {
            //show hidden form
            $('.datatable-container').hide();
            $('.panel_form').show();
            $('.panel_form .panel-body').html(data)
        }
    })
});
$(document).on('click','.cancel_button',function (e) {
    e.preventDefault();
    $('.panel_form').hide();
    $('.datatable-container').show();
});

$(document).on('click','.btn_route_generate',function (e) {
    e.preventDefault();
    if ($('#route_generator').val() !== ''){
            $.ajax({
                url: $(this).data('url'),
                type: 'GET',
                data: {route: $('#route_generator').val()},
                success: function (data) {
                    $('#route').val(data)
                },
                error: function (xhr) {
                    toastMessage("Oopps!",xhr.responseJSON.message)
                }
        })
    }
});

