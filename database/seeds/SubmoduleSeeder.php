<?php

use Illuminate\Database\Seeder;

class SubmoduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Admin modules
        \App\SubModule::create([
            'module_id' => '1',
            'submodule_name' => 'Dashboard',
            'user_group' => '1',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '1',
            'submodule_name' => 'Company Structure',
            'user_group' => '1',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '1',
            'submodule_name' => 'Job Details Setup',
            'user_group' => '1',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '1',
            'submodule_name' => 'Project/Client Setup',
            'user_group' => '1',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '1',
            'submodule_name' => 'Overtime Management',
            'user_group' => '1',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '1',
            'submodule_name' => 'Leave Management',
            'user_group' => '1',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '2',
            'submodule_name' => 'Employees',
            'user_group' => '1',
            'route' => '1'
        ]);
        \App\SubModule::create([
            'module_id' => '2',
            'submodule_name' => 'Monitor Attendance',
            'user_group' => '1',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '3',
            'submodule_name' => 'Reports',
            'user_group' => '1',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '4',
            'submodule_name' => 'Settings',
            'user_group' => '1',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '4',
            'submodule_name' => 'Users',
            'user_group' => '1',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '4',
            'submodule_name' => 'Manage Modules',
            'user_group' => '1',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '4',
            'submodule_name' => 'Manage Permissions',
            'user_group' => '1',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '5',
            'submodule_name' => 'Salary',
            'user_group' => '1',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '5',
            'submodule_name' => 'Manage Payroll',
            'user_group' => '1',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '5',
            'submodule_name' => 'Payroll Reports',
            'user_group' => '1',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '5',
            'submodule_name' => 'Payroll Settings',
            'user_group' => '1',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '6',
            'submodule_name' => 'Dashboard',
            'user_group' => '2',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '6',
            'submodule_name' => 'Personal Information',
            'user_group' => '2',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '6',
            'submodule_name' => 'Dependents',
            'user_group' => '2',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '6',
            'submodule_name' => 'Qualification',
            'user_group' => '2',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '6',
            'submodule_name' => 'Emergency Contacts',
            'user_group' => '2',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '7',
            'submodule_name' => 'Attendance',
            'user_group' => '2',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '7',
            'submodule_name' => 'Time Sheet',
            'user_group' => '2',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '7',
            'submodule_name' => 'Overtime',
            'user_group' => '2',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '7',
            'submodule_name' => 'Leave',
            'user_group' => '2',
            'route' => ''
        ]);
        \App\SubModule::create([
            'module_id' => '8',
            'submodule_name' => 'Staff Directory',
            'user_group' => '2',
            'route' => ''
        ]);

    }
}
