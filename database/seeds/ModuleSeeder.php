<?php

use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Module::create([
            'module_name' => 'Admin',
            'icon' => 'fa fa-cubes'
        ]);

        \App\Module::create([
            'module_name' => 'Employees',
            'icon' => 'fa fa-users'
        ]);

        \App\Module::create([
            'module_name' => 'Admin Reports',
            'icon' => 'fa fa-folder-open-o'
        ]);

        \App\Module::create([
            'module_name' => 'System',
            'icon' => 'fa fa-cogs'
        ]);

        \App\Module::create([
            'module_name' => 'Payroll',
            'icon' => 'fa fa-calculator'
        ]);

        \App\Module::create([
            'module_name' => 'My Profile',
            'icon' => 'fa fa-user'
        ]);

        \App\Module::create([
            'module_name' => 'Time Management',
            'icon' => 'fa fa-clock-o'
        ]);

        \App\Module::create([
            'module_name' => 'Company',
            'icon' => 'fa fa-building'
        ]);
    }
}
