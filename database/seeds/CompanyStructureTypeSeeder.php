<?php

use Illuminate\Database\Seeder;

class CompanyStructureTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\CompanyStructureType::create([
            'type_name' => 'Company',
            'hierarchy' => 1
        ]);
        \App\CompanyStructureType::create([
            'type_name' => 'Head Office',
            'hierarchy' => 2
        ]);
        \App\CompanyStructureType::create([
            'type_name' => 'Regional Office',
            'hierarchy' => 2
        ]);
        \App\CompanyStructureType::create([
            'type_name' => 'Department',
            'hierarchy' => 3
        ]);
    }
}
