<?php

use Illuminate\Database\Seeder;

class UserLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\UserLevel::create([
            'user_level' => 'Admin'
        ]);
        \App\UserLevel::create([
            'user_level' => 'Manager'
        ]);
        \App\UserLevel::create([
            'user_level' => 'Employee'
        ]);
    }
}
