<?php

use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Department::create([
            'department_name' => 'Software Development'
        ]);
        \App\Department::create([
            'department_name' => 'HR and Procurement'
        ]);
        \App\Department::create([
            'department_name' => 'Sales'
        ]);
        \App\Department::create([
            'department_name' => 'Management'
        ]);
        \App\Department::create([
            'department_name' => 'Infrastracture and Engineering'
        ]);
        \App\Department::create([
            'department_name' => 'Utility'
        ]);
    }
}
