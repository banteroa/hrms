<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();
//Route::resource('login','LoginController');
Route::middleware(['auth'])->group(function (){
    Route::resource('sub_module','SubModuleController');
    Route::get('generate/route','HomeController@generateRoute')->name('generate.route');
    Route::resource('company_structure','CompanyStructureController');
    Route::resource('job_detail','JobDetailController');
    Route::resource('overtime_management','OvertimeController');
    Route::resource('leave_management','LeaveManagementController');
    Route::resource('attendance','AttendanceController');
    Route::resource('users','UserController');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/employees','EmployeeController');
