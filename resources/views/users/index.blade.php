@extends('home')
@section('main-content')
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="entypo-folder"></i>
                Home
            </a>
        </li>
        <li><a href="#">System</a></li>
        <li class="active"><strong>Users</strong></li>
    </ol>
    <hr>

    <div class="right" style="float:right">
        <a class="btn btn-primary btn-icon icon-left" href="{{ route('users.create') }}" id="">
            <i class="entypo-plus"></i>
            Add New
        </a>
    </div>

    <table class="table table-bordered datatable" id="table-users">
        <thead>
        <tr>
            <th>ID</th>
            <th data-hide="phone">Username</th>
            <th>Authentication Email</th>
            <th data-hide="phone">Employee</th>
            <th data-hide="phone,tablet">User Level</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
@endsection
@push('scripts')
    <script>
        $(document).ready(function (e) {
            let $table = $('#table-users');
            $table.DataTable({
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bStateSave": true
            });

            // $table.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
            //     minimumResultsForSearch: -1
            // });
        })
    </script>
@endpush