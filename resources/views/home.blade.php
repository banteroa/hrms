@extends('layouts.app')

@section('content')
    <body class="page-body page-left-in">
        <div class="page-container">
            @include('layouts.sidebar')
            <div class="main-content">
                @include('layouts.topbar')
                @yield('main-content')
            </div>
        </div>
    </body>
@endsection
