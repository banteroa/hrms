@extends('home')
@section('main-content')
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="entypo-folder"></i>
                Home
            </a>
        </li>
        <li><a href="#">Employees</a></li>
        <li class="active"><strong>Add Employee</strong></li>
    </ol>
    <hr>
    <div class="well well-sm">
        <h4>Please fill the details to register new employee.</h4>
    </div>


    <form id="rootwizard-2" method="post" action="" class="form-wizard form-horizontal validate">

        <div class="steps-progress">
            <div class="progress-indicator"></div>
        </div>

        <ul>
            <li class="active">
                <a href="#tab2-1" data-toggle="tab"><span>1</span>Personal Info</a>
            </li>
            <li>
                <a href="#tab2-2" data-toggle="tab"><span>2</span>Contact Information</a>
            </li>
            <li>
                <a href="#tab2-3" data-toggle="tab"><span>3</span>Education</a>
            </li>
            <li>
                <a href="#tab2-4" data-toggle="tab"><span>4</span>Job Details</a>
            </li>
            <li>
                <a href="#tab2-5" data-toggle="tab"><span>5</span>Register</a>
            </li>
        </ul>

        <div class="tab-content">

            @include('employees.include.create_step_1')

            @include('employees.include.create_step_2')

            @include('employees.include.create_step_3')

            @include('employees.include.create_step_4')

            @include('employees.include.create_step_5')

            <ul class="pager wizard">
                <li class="previous">
                    <a href="#"><i class="entypo-left-open"></i> Previous</a>
                </li>

                <li class="next">
                    <a href="#">Next <i class="entypo-right-open"></i></a>
                </li>
            </ul>
        </div>

    </form>
@endsection
