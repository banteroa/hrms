<div class="tab-pane " id="tab2-1">
        {{--add data-validate="required"--}}
    <div class="panel panel-primary panel_form " >
        <div class="panel-heading">
            <div class="panel-title">Employee Basic Information</div>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="first_name" class="col-sm-3 control-label">First Name</label>

                <div class="col-sm-5">
                    <input type="text" class="form-control" id="first_name" name="first_name"  placeholder=""  />
                </div>
            </div>
            <div class="form-group">
                <label for="middle_name" class="col-sm-3 control-label">Middle Name</label>

                <div class="col-sm-5">
                    <input type="text" class="form-control" id="middle_name" name="middle_name"  placeholder="" />
                </div>
            </div>
            <div class="form-group">
                <label for="last_name" class="col-sm-3 control-label">Last Name</label>

                <div class="col-sm-5">
                    <input type="text" class="form-control" id="last_name" name="last_name"  placeholder="" />
                </div>
            </div>

            <div class="form-group">
                <label for="employee_id" class="col-sm-3 control-label">Employee ID</label>

                <div class="col-sm-5">
                    <input type="text" class="form-control" id="employee_id" name="employee_id"  placeholder="" />
                </div>
            </div>

            <div class="form-group">
                <label for="date_of_birth" class="col-sm-3 control-label">Date of Birth</label>

                <div class="col-sm-5">
                    <input type="text" class="form-control" id="date_of_birth" name="date_of_birth"  placeholder="" />
                </div>
            </div>

            <div class="form-group">
                <label for="gender" class="col-sm-3 control-label">Gender</label>

                <div class="col-sm-5">
                    <select class="form-control" name="gender" id="gender">
                        <option value="">Select Gender</option>
                        <option value="1">Male</option>
                        <option value="2">Female</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="marital" class="col-sm-3 control-label">Marital Status</label>

                <div class="col-sm-5">
                    <select class="form-control" name="marital" id="marital">
                        <option value="">Select Marital Status</option>
                        <option value="1">Single</option>
                        <option value="2">Married</option>
                        <option value="3">Divorced</option>
                        <option value="4">Separated</option>
                    </select>
                </div>
            </div>
        </div>
    </div>

</div>