<div class="tab-pane active" id="tab2-1">
    <div class="row">
        <div class="form-group">
            <label class="col-sm-3 control-label">Image Upload</label>

            <div class="col-sm-5">

                <div class="fileinput fileinput-new" data-provides="fileinput"><input type="hidden">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                        <img src="http://placehold.it/200x150" alt="...">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                    <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="..." accept="image/*">
											</span>
                        <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label" for="full_name">First Name</label>
                <input class="form-control" name="first_name" id="first_name" data-validate="required" placeholder="John">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label" for="full_name">Middle Name</label>
                <input class="form-control" name="middle_name" id="middle_name" data-validate="required" placeholder="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label" for="full_name">Last Name</label>
                <input class="form-control" name="last_name" id="last_name" data-validate="required" placeholder="Doe">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label" for="dob">Date of Birth</label>
                <input class="form-control" name="dob" id="dob" data-validate="required" placeholder="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label" for="birth_place">Birth Place</label>
                <input class="form-control" name="birth_place" id="birth_place" data-validate="required" placeholder="">
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label" for="about">Write Something About You</label>
                <textarea class="form-control autogrow" name="about" id="about" data-validate="minlength[10]" rows="5" placeholder="Could be used also as Motivation Letter" style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 98px;"></textarea>
            </div>
        </div>

    </div>
</div>