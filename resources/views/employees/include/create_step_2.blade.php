<div class="tab-pane " id="tab2-2">
    <div class="panel panel-primary panel_form " >
        <div class="panel-heading">
            <div class="panel-title">Contact Information</div>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="address_line_1" class="col-sm-3 control-label">Address Line 1</label>

                <div class="col-sm-5">
                    <textarea class="form-control autogrow" name="address_line_1" id="address_line_1" placeholder="I will grow as you type new lines." style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 115px;"></textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="address_line_2" class="col-sm-3 control-label">Address Line 2 (Optional)</label>

                <div class="col-sm-5">
                    <textarea class="form-control autogrow" name="address_line_2" id="address_line_2" placeholder="I will grow as you type new lines." style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 115px;"></textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="mobile_number" class="col-sm-3 control-label">Mobile Number</label>

                <div class="col-sm-5">
                    <input type="text" class="form-control" id="mobile_number" name="mobile_number"  placeholder=""  />
                </div>
            </div>

            <div class="form-group">
                <label for="telephone_number" class="col-sm-3 control-label">Telephone Number (Optional)</label>

                <div class="col-sm-5">
                    <input type="text" class="form-control" id="telephone_number" name="telephone_number"  placeholder=""  />
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Email</label>

                <div class="col-sm-5">
                    <input type="text" class="form-control" id="email" name="email"  placeholder=""  />
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="panel-title">Emergency Contacts</div>
        </div>
        <div class="panel-body">
            <div class="form-group" style="margin-top: 10px;">
                <span style="position: absolute;top: 8px;left: 652px;"><i class="fa fa-plus"></i> Add new</span>
                <label for="contact_person1" class="col-sm-3 control-label">Name</label>

                <div class="col-sm-5">
                    <input class="form-control" type="text" id="contact_person1" name="contact_person1">
                </div>
            </div>
            <div class="form-group">
                <label for="relationship1" class="col-sm-3 control-label">Relationship</label>

                <div class="col-sm-5">
                    <input class="form-control" type="text" name="relationship1" id="relationship1">
                </div>
            </div>
            <div class="form-group">
                <label for="contact_number1" class="col-sm-3 control-label">Contact Number</label>

                <div class="col-sm-5">
                    <input class="form-control" type="text" name="contact_number1" id="contact_number1">
                </div>
            </div>
        </div>
        <div class="panel-body">

        </div>
    </div>
</div>
