<div class="tab-pane active" id="tab2-5">

    <div class="panel panel-primary panel_form " >
        <div class="panel-heading">
            <div class="panel-title">Job Details</div>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="employment_status" class="col-sm-3 control-label">Choose Username</label>

                <div class="col-sm-5">
                    <input type="text" class="form-control" placeholder="Could also be your email">
                </div>
            </div>
            <div class="form-group">
                <label for="employment_status" class="col-sm-3 control-label">Password</label>

                <div class="col-sm-5">
                    <input type="text" class="form-control" placeholder="Enter strong password">
                </div>
            </div>
            <div class="form-group">
                <label for="employment_status" class="col-sm-3 control-label">Confirm Password</label>

                <div class="col-sm-5">
                    <input type="text" class="form-control" placeholder="">
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Finish Registration</button>
    </div>

</div>