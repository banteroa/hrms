<div class="tab-pane " id="tab2-3">
    <div class="panel panel-primary panel_form " >
        <div class="panel-heading">
            <div class="panel-title">Primary School</div>
        </div>
        <div class="panel-body">
            <div class="form-group">

                <div class="col-sm-5">
                    <label for="primary_school" class=" control-label">School Name</label>

                    <input class="form-control" name="prim_subject" id="prim_subject" data-validate="require" placeholder="Graduation Degree" />
                </div>
                <div class="col-md-3">
                    <label class="control-label" for="prim_date_start">Start Date</label>
                    <input class="form-control datepicker" name="prim_date_start" id="prim_date_start" data-start-view="2" placeholder="(Optional)" />
                </div>

                <div class="col-md-3">
                    <label class="control-label" for="prim_date_end">End Date</label>
                    <input class="form-control datepicker" name="prim_date_end" id="prim_date_end" data-start-view="2" placeholder="(Optional)" />
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-primary panel_form " >
        <div class="panel-heading">
            <div class="panel-title">Secondary School</div>
        </div>
        <div class="panel-body">
            <div class="form-group">

                <div class="col-sm-5">
                    <label for="primary_school" class=" control-label">School Name</label>

                    <input class="form-control" name="prim_subject" id="prim_subject" data-validate="require" placeholder="Graduation Degree" />
                </div>
                <div class="col-md-3">
                    <label class="control-label" for="prim_date_start">Start Date</label>
                    <input class="form-control datepicker" name="prim_date_start" id="prim_date_start" data-start-view="2" placeholder="(Optional)" />
                </div>

                <div class="col-md-3">
                    <label class="control-label" for="prim_date_end">End Date</label>
                    <input class="form-control datepicker" name="prim_date_end" id="prim_date_end" data-start-view="2" placeholder="(Optional)" />
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-primary panel_form " >
        <div class="panel-heading">
            <div class="panel-title">Tertiary School</div>
        </div>
        <div class="panel-body">
            <div class="form-group">

                <div class="col-sm-5">
                    <label for="primary_school" class=" control-label">School Name</label>

                    <input class="form-control" name="prim_subject" id="prim_subject" data-validate="require" placeholder="Graduation Degree" />
                </div>
                <div class="col-md-3">
                    <label class="control-label" for="prim_date_start">Start Date</label>
                    <input class="form-control datepicker" name="prim_date_start" id="prim_date_start" data-start-view="2" placeholder="(Optional)" />
                </div>

                <div class="col-md-3">
                    <label class="control-label" for="prim_date_end">End Date</label>
                    <input class="form-control datepicker" name="prim_date_end" id="prim_date_end" data-start-view="2" placeholder="(Optional)" />
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-primary panel_form " >
        <div class="panel-heading">
            <div class="panel-title">Certifications</div>
        </div>
        <div class="panel-body">
            <div class="form-group">

                <div class="col-sm-4">
                    <label for="primary_school" class=" control-label">Certification Name</label>

                    <input class="form-control" name="prim_subject" id="prim_subject" data-validate="require" placeholder="Graduation Degree" />
                </div>
                <div class="col-sm-4">
                    <label for="primary_school" class=" control-label">Institute</label>

                    <input class="form-control" name="prim_subject" id="prim_subject" data-validate="require" placeholder="Graduation Degree" />
                </div>
                <div class="col-md-2">
                    <label class="control-label" for="prim_date_start">Start Date</label>
                    <input class="form-control datepicker" name="prim_date_start" id="prim_date_start" data-start-view="2" placeholder="(Optional)" />
                </div>

                <div class="col-md-2">
                    <label class="control-label" for="prim_date_end">End Date</label>
                    <input class="form-control datepicker" name="prim_date_end" id="prim_date_end" data-start-view="2" placeholder="(Optional)" />
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-primary panel_form " >
        <div class="panel-heading">
            <div class="panel-title">Others</div>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <div class="col-md-6">
                    <label class="control-label" for="prim_date_end">Skills</label>
                    <input class="form-control " name="" id=""  placeholder="(Optional)" />
                </div>
                <div class="col-md-6">
                    <label class="control-label" for="prim_date_end">Languages</label>
                    <input class="form-control " name="" id=""  placeholder="(Optional)" />
                </div>
            </div>
        </div>
    </div>
</div>