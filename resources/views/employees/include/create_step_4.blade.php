<div class="tab-pane " id="tab2-4">
    <div class="panel panel-primary panel_form " >
        <div class="panel-heading">
            <div class="panel-title">Job Details</div>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="employment_status" class="col-sm-3 control-label">Employment Status</label>

                <div class="col-sm-5">
                    <select name="employment_status" id="employment_status" class="form-control">
                        <option value="">Select here</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Office</label>

                <div class="col-sm-5">
                    <select name="" id="" class="form-control">
                        <option value="">Select here</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Department</label>

                <div class="col-sm-5">
                    <select name="" id="" class="form-control">
                        <option value="">Select here</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Job Title</label>

                <div class="col-sm-5">
                    <select name="" id="" class="form-control">
                        <option value="">Select here</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Salary</label>

                <div class="col-sm-5">
                    <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Joined Date</label>

                <div class="col-sm-5">
                    <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Termination Date</label>

                <div class="col-sm-5">
                    <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Supervisor</label>

                <div class="col-sm-5">
                    <select name="" id="" class="form-control">
                        <option value="">Select here</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Approver 1</label>

                <div class="col-sm-5">
                    <select name="" id="" class="form-control">
                        <option value="">Select here</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Approver 2</label>

                <div class="col-sm-5">
                    <select name="" id="" class="form-control">
                        <option value="">Select here</option>
                    </select>
                </div>
            </div>

        </div>
    </div>
    <div class="panel panel-primary panel_form " >
        <div class="panel-heading">
            <div class="panel-title">Work Experience</div>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <div class="col-sm-1">
                    <label class="control-label">&nbsp;</label>
                    <p class="text-right">
                        <span class="label label-info">1</span>
                    </p>
                </div>
                <div class="col-sm-4">
                    <label for="primary_school" class=" control-label">Company Name</label>

                    <input class="form-control" name="prim_subject" id="prim_subject" data-validate="require" placeholder="Graduation Degree" />
                </div>
                <div class="col-sm-3">
                    <label for="primary_school" class=" control-label">Job Title</label>

                    <input class="form-control" name="prim_subject" id="prim_subject" data-validate="require" placeholder="Graduation Degree" />
                </div>
                <div class="col-md-2">
                    <label class="control-label" for="prim_date_start">Start Date</label>
                    <input class="form-control datepicker" name="prim_date_start" id="prim_date_start" data-start-view="2" placeholder="(Optional)" />
                </div>

                <div class="col-md-2">
                    <label class="control-label" for="prim_date_end">End Date</label>
                    <input class="form-control datepicker" name="prim_date_end" id="prim_date_end" data-start-view="2" placeholder="(Optional)" />
                </div>
            </div>
        </div>
    </div>

</div>