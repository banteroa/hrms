@extends('home')
@section('main-content')
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="entypo-folder"></i>
                Home
            </a>
        </li>
        <li><a href="#">Employees</a></li>
        <li class="active"><strong>Employee List</strong></li>
    </ol>
    <hr>
    <div class="right" style="float:right">
        <a class="btn btn-primary btn-icon icon-left" href="{{ route('employees.create') }}" id="">
            <i class="entypo-plus"></i>
            Add New
        </a>
    </div>
    <table class="table table-bordered datatable" id="table-1">
        <thead>
        <tr>
            <th>Image</th>
            <th data-hide="phone">Employee ID</th>
            <th>Last Name</th>
            <th data-hide="phone">First Name</th>
            <th data-hide="phone,tablet">Department</th>
            <th>Position</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
            @foreach($employees as $employee)
                <tr class="">
                    <td></td>
                    <td>{{ $employee->id }}</td>
                    <td>{{ $employee->last_name }}</td>
                    <td>{{ $employee->first_name }}</td>
                    <td class="center">4</td>
                    <td class="center">X</td>
                    <td class="center">X</td>
                </tr>
            @endforeach
        </tbody>
    </table>


@endsection
@push('scripts')
    <script>
        $(document).ready(function (e) {
            let $table = $('#table-1');
            $table.DataTable({
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bStateSave": true
            });

            // $table.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
            //     minimumResultsForSearch: -1
            // });
        })
    </script>
@endpush
