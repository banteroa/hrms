<form id="edit_submodule" action="{{ route('sub_module.update',$sub_module->id) }}" method="PUT" role="form" class="form-horizontal validate" >
        @csrf
        <div class="form-group">
        <label for="submodule_name" class="col-sm-3 control-label">Sub Module Name</label>

        <div class="col-sm-5">
            <input type="text" class="form-control" id="submodule_name" name="submodule_name"  placeholder="" value="{{ $sub_module->submodule_name }}"/>
        </div>
    </div>
    <div class="form-group">
        <label for="status" class="col-sm-3 control-label">Status</label>
        <div class="col-sm-5">
            <select name="status" id="status" class="form-control">
                <option value="1" {{ ($sub_module->is_enabled == 1 ? "selected" : "") }}>Enabled</option>
                <option value="2" {{ ($sub_module->is_enabled == 2 ? "selected" : "") }}>Disabled</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="module_id" class="col-sm-3 control-label">Parent Module</label>
        <div class="col-sm-5">
            <select name="module_id" id="module_id" class="form-control">
                @foreach($modules as $module)
                    <option value="{{ $module->id }}" {{ ($module->id == $sub_module->module_id ? "selected":"") }}>{{ $module->module_name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="route_generator" class="col-sm-3 control-label">Generate Route</label>
        <div class="col-sm-5">
            <div class="input-group">
                <input type="text" class="form-control" id="route_generator">

                <span class="input-group-btn">
											<button class="btn btn-primary btn_route_generate" type="button" data-url="{{ route('generate.route') }}">Go!</button>
										</span>
            </div>
        </div>


    </div>
    <div class="form-group">
        <label for="route" class="col-sm-3 control-label">Route</label>
        <div class="col-sm-5">
            <input type="text" name="route" id="route" class="form-control" value="{{ $sub_module->route }}" readonly>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-6">
            <button type="button" class="btn btn-default btn-icon icon-left cancel_button">
                Cancel
                <i class="entypo-cancel"></i>
            </button>
            <button type="submit" class="btn btn-blue btn-icon icon-left button-submit">
                Save
                <i class=""><span class="entypo-check"></span></i>
            </button>
        </div>
    </div>
</form>
<script>
    let rules = {
        sample_id : {
            required: true
        },
        submodule_name: {
            required: true
        }
    };
    neonLogin.validateForm('#edit_submodule',rules);
</script>