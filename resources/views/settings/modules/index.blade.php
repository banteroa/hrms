@extends('home')
@section('main-content')
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="entypo-folder"></i>
                Home
            </a>
        </li>
        <li><a href="#">System</a></li>
        <li class="active"><strong>Manage Modules</strong></li>
    </ol>
    <hr>
    <div class="row datatable-container" >
        <table class="table table-bordered datatable" id="table-permissions"  >
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th data-hide="phone">Parent Module</th>
                <th>Group</th>
                <th data-hide="phone,tablet">Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($sub_modules as $sub_module)
                <tr>
                    <td>{{ $sub_module->id }}</td>
                    <td>{{ $sub_module->submodule_name }}</td>
                    <td>{{ $sub_module->getModuleParent->module_name }}</td>
                    <td>{{ ($sub_module->group == 1 ? "admin":"user") }}</td>
                    <td>{{ ($sub_module->is_enabled == 1 ? "Enabled" : "Disabled") }}</td>
                    <td class="cent">
                        <a href="#" class="btn btn-default btn-sm btn-icon icon-left edit_button" data-url="{{ route('sub_module.edit',$sub_module->id) }}">
                            <i class="entypo-pencil"></i>
                            Edit
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary panel_form " style="display: none;">
                <div class="panel-heading">
                    <div class="panel-title">Title</div>
                </div>
                <div class="panel-body">

                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function (e) {
            let $table = $('#table-permissions');
            $table.DataTable({
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bStateSave": true
            });
            // $table.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
            //     minimumResultsForSearch: -1
            // });
        })
    </script>
@endpush
