<div class="right" style="float:right">
    <a class="btn btn-primary btn-icon icon-left" href="{{ route('job_detail.create') }}" id="">
        <i class="entypo-plus"></i>
        Add New
    </a>
</div>
<div class="row datatable-container" >
    <table class="table table-bordered datatable" id="table-OT-requests"  >
        <thead>
        <tr>
            <th>Employee</th>
            <th>Time In</th>
            <th>Time Out</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
@push('scripts')
    <script>
        $(document).ready(function (e) {
            let $table = $('#table-OT-requests');
            $table.DataTable({
                "aLengthMenu": [[20, 25, 50, -1], [10, 25, 50, "All"]],
                "bStateSave": true
            });
        })
    </script>
@endpush