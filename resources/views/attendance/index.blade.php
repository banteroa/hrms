@extends('home')
@section('main-content')
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="entypo-folder"></i>
                Home
            </a>
        </li>
        <li><a href="#">Admin</a></li>
        <li class="active"><strong>Monitor Attendance</strong></li>
    </ol>
    <hr>

    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs bordered">
                <li class="active">
                    <a href="#job_titles" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="entypo-home"></i></span> <span class="hidden-xs">Monitor Attendance</span> </a>
                </li>
                <li class="">
                    <a href="#employment_status" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="entypo-user"></i></span> <span class="hidden-xs">Today's Attendance</span> </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="job_titles">
                    @include('attendance.include.attendance')
                </div>

                <div class="tab-pane" id="employment_status">
                    @include('attendance.include.current')
                </div>
            </div>
        </div>

    </div>
@endsection