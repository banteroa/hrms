@extends('home')
@section('main-content')
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="entypo-folder"></i>
                Home
            </a>
        </li>
        <li><a href="#">Admin</a></li>
        <li class="active"><strong>Overtime Management</strong></li>
    </ol>
    <hr>

    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs bordered">
                <li class="active">
                    <a href="#job_titles" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="entypo-home"></i></span> <span class="hidden-xs">Overtime Categories</span> </a>
                </li>
                <li class="">
                    <a href="#employment_status" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="entypo-user"></i></span> <span class="hidden-xs">Overtime Requests</span> </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="job_titles">
                    @include('admin.overtime_management.include.categories')
                </div>

                <div class="tab-pane" id="employment_status">
                    @include('admin.overtime_management.include.requests')
                </div>
            </div>
        </div>
    </div>
@endsection