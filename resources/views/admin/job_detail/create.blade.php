@extends('home')
@section('main-content')
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="entypo-folder"></i>
                Home
            </a>
        </li>
        <li><a href="#">Admin</a></li>
        <li><a href="#">Job Titles</a></li>
        <li class="active"><strong>Add New</strong></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary panel_form " >
                <div class="panel-heading">
                    <div class="panel-title">Add New Job Title</div>
                </div>
                <div class="panel-body">
                    <form id="new_structure_form" action="{{ route('job_detail.store') }}" method="POST" role="form" class="form-horizontal " >
                        @csrf
                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Job Title Code</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="code" name="code"  placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-3 control-label">Job Title Name</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="name" name="name"  placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="department" class="col-sm-3 control-label">Department</label>
                            <div class="col-sm-5">
                                <select name="department" id="department" class="form-control">
                                    <option value="">Select Here</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-sm-3 control-label">Job Title Description</label>
                            <div class="col-sm-5">
                                <textarea class="form-control autogrow" name="description" id="description" placeholder="I will grow as you type new lines." style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 114.8px;"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="specification" class="col-sm-3 control-label">Job Title Specification</label>
                            <div class="col-sm-5">
                                <textarea class="form-control autogrow" name="specification" id="specification" placeholder="I will grow as you type new lines." style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 114.8px;"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-8" >
                                {{--<button type="button" class="btn btn-default btn-icon icon-left cancel_button">--}}
                                {{--Cancel--}}
                                {{--<i class="entypo-cancel"></i>--}}
                                {{--</button>--}}
                                <button style="float: right;" type="submit" class="btn btn-blue btn-icon icon-left button-submit">
                                    Save
                                    <i class=""><span class="entypo-check"></span></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection