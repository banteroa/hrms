@extends('home')
@section('main-content')
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="entypo-folder"></i>
                Home
            </a>
        </li>
        <li><a href="#">Admin</a></li>
        <li><a href="#">Company Structure</a></li>
        <li class="active"><strong>Add New</strong></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary panel_form " >
                <div class="panel-heading">
                    <div class="panel-title">Add New Company Structure</div>
                </div>
                <div class="panel-body">
                    <form id="new_structure_form" action="{{ route('company_structure.store') }}" method="POST" role="form" class="form-horizontal " >
                        @csrf
                        <div class="form-group">
                            <label for="name" class="col-sm-3 control-label">Name</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="name" name="name"  placeholder="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="details" class="col-sm-3 control-label">Details</label>
                            <div class="col-sm-5">
                                <textarea class="form-control autogrow" name="details" id="details" placeholder="I will grow as you type new lines." style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 114.8px;"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="col-sm-3 control-label">Address</label>
                            <div class="col-sm-5">
                                <textarea class="form-control autogrow" name="address" id="address" placeholder="I will grow as you type new lines." style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 114.8px;"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="type" class="col-sm-3 control-label">Type</label>
                            <div class="col-sm-5">
                                <select name="type" id="type" class="form-control">
                                    <option value="">Select Here</option>
                                    @foreach($types as $type)
                                        <option value="{{ $type->id }}">{{ $type->type_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parent_structure" class="col-sm-3 control-label">Parent Structure</label>
                            <div class="col-sm-5">
                                <select name="parent_structure" id="parent_structure" class="form-control">
                                    <option value="">Select Here</option>
                                    @foreach($parents as $parent)
                                        <option value="{{ $parent->id }}">{{ $parent->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8" >
                                {{--<button type="button" class="btn btn-default btn-icon icon-left cancel_button">--}}
                                    {{--Cancel--}}
                                    {{--<i class="entypo-cancel"></i>--}}
                                {{--</button>--}}
                                <button style="float: right;" type="submit" class="btn btn-blue btn-icon icon-left button-submit">
                                    Save
                                    <i class=""><span class="entypo-check"></span></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            let rules = {
                name: {
                    required: true
                }
            };
            neonLogin.validateForm('#new_structure_form',rules)
        })


    </script>
@endpush

