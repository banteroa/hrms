@extends('home')
@section('main-content')
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="entypo-folder"></i>
                Home
            </a>
        </li>
        <li><a href="#">Admin</a></li>
        <li class="active"><strong>Company Structure</strong></li>
    </ol>
    <hr>

    <div class="right" style="float:right">
        <a class="btn btn-primary btn-icon icon-left" href="{{ route('company_structure.create') }}" id="">
            <i class="entypo-plus"></i>
            Add New
        </a>
    </div>
    <div class="row datatable-container" >
        <table class="table table-bordered datatable" id="table-structure"  >
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th data-hide="phone">Address</th>
                <th>Type</th>
                <th data-hide="phone,tablet">Parent Structure</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @foreach($structures as $structure)
                    <tr>
                        <td>{{ $structure->id }}</td>
                        <td>{{ $structure->name }}</td>
                        <td>{{ $structure->address }}</td>
                        <td>{{ $structure->getTypeIdParent->type_name }}</td>
                        <td>{{ ($structure->parent_id != null ? $structure->getParent->name : "-") }}</td>
                        <td class="cent">
                            <a href="#" class="btn btn-default btn-sm btn-icon icon-left edit_button" data-url="{{ route('company_structure.edit',$structure->id) }}">
                                <i class="entypo-pencil"></i>
                                Edit
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function (e) {
            let $table = $('#table-structure');
            $table.DataTable({
                "aLengthMenu": [[20, 25, 50, -1], [10, 25, 50, "All"]],
                "bStateSave": true
            });
            // $table.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
            //     minimumResultsForSearch: -1
            // });
        })
    </script>
@endpush